#include "dialog.h"
#include "ui_dialog.h"
#include <QFont>

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);

    for(int i = 0; i < 10; i++)
    {
        ui->listWidget->addItem("Opción " + QString(QChar('A' + i)));
    }
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::on_pushButton_clicked()
{
    QListWidgetItem * item = ui->listWidget->currentItem();
    item->setText("Te cambie!");
    item->setBackground(Qt::green);
    item->setForeground(Qt::red);
    item->setFont(QFont("Arial", 11, QFont::Bold));
}
